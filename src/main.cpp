/////////////////////////////////////////////////////////////////////
// v4l-utils testing code,
// for RK3399 + AR0521 (monochrome)
// -----------------------------------------------------------------
//                     programmed by Raphael Kim, rageworx@gmail.com
/////////////////////////////////////////////////////////////////////

#include <unistd.h>
#include <signal.h>

#include <cstdio>
#include <cstdlib>

#include <fcntl.h>
#include <cstring>
#include <errno.h>
#include <sys/mman.h>
#include <sys/ioctl.h>

#include "v4l-helpers.h"
#include <jpeglib.h>

#include <semaphore.h>
#include <pthread.h>
#include <omp.h>

#include <FL/x.H>
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Image.H>
#include <FL/Fl_RGB_Image.H>
#include <FL/Fl_PNG_Image.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Check_Button.H>
#include "Fl_VSliderEx.H"
#include "fl_imgtk.h"

#include <vector>
#include <string>

#include "tick.h"
#include "version.h"

//////////////////////////////////////////////////////////////////////

#define CLEAR(x)        memset(&(x),0,sizeof(x))
#define REQ_BUFF_SZ     8
#define NILL            (void*)(0xffffffffffffffff)
#define SEM_NAME        "MCSI2MON_FLTK"
#define SIG_CAP_L       SIGUSER1
#define SIG_CAP_R       SIGUSER2
#define SUPPORTED_FMT   0x47425247      /// GBRG bayer array.
#define EXPOSURE_R      0.005f

//////////////////////////////////////////////////////////////////////

using namespace std;

//////////////////////////////////////////////////////////////////////

typedef struct _threadopt{
    unsigned devindex;
    void*    param;
}threadopt;

typedef struct _imgSepc{
    unsigned width;
    unsigned height;
    unsigned depth;
}imgSpec;

//////////////////////////////////////////////////////////////////////

static pthread_t            ptGrab[2] = {0};
static pthread_mutex_t      ptMtx[2] = {PTHREAD_MUTEX_INITIALIZER};
static pthread_cond_t       ptSig[2] = {PTHREAD_COND_INITIALIZER};
static pthread_t            ptRender = 0;
static bool                 threadalive = true;
static bool                 bufferGrabLock[2] = {false,};
static uchar*               bufferGrab[2] = {NULL};
static imgSpec              bufferSpec[2] = {0};
static Fl_Window*           winMain = NULL;
static Fl_Box*              boxCamera[2] = {NULL};
static Fl_Box*              boxCOverlay[2] = {NULL};
static Fl_RGB_Image*        imgCamera[2] = {NULL};
static Fl_Box*              boxPerf[2] = {NULL};
static Fl_Button*           btnCapture = NULL;
static Fl_Button*           btnGrabRaw = NULL;
static Fl_VSliderEx*        sldExposure = NULL;
static Fl_VSliderEx*        sldGain = NULL;
static Fl_Box*              boxCR = NULL;
static Fl_Check_Button*     btnZoom = NULL;
static Fl_Check_Button*     btnHDR = NULL;
static Fl_RGB_Image*        imgIcon = NULL;

static unsigned             grabPerf[2];
static char                 strPerf[2][80] = {0};
static bool                 sigmutex = false;
static sem_t*               semthis = NULL;
static char                 strCapPath[128] = {0};
static string               strCapError[2];
static bool                 enableCtl[2] = {false};

static struct v4l_fd        camera_fd[2];
struct v4l2_query_ext_ctrl* extCtlExposure[2] = {NULL};
struct v4l2_query_ext_ctrl* extCtlGain[2] = {NULL};

//////////////////////////////////////////////////////////////////////

void fl_wcb( Fl_Widget* w, void* p );
void fl_tcb( void* p );
void sigHandler( int Sig );

//////////////////////////////////////////////////////////////////////

int create_jpeg( const char* fname, unsigned char *img_src, size_t w, size_t h, size_t d )
{
    FILE *out;
    struct jpeg_compress_struct jcs;
    struct jpeg_error_mgr jem;

    JSAMPROW row_pointer[1];
    jcs.err = jpeg_std_error(&jem);
    jpeg_create_compress(&jcs);

    out = fopen( fname,"wb" );
    jpeg_stdio_dest(&jcs, out);

    jcs.image_width = w;
    jcs.image_height = h;
    jcs.input_components = d;
    switch( d )
    {
        case 1:
            jcs.in_color_space = JCS_GRAYSCALE;
            break;

        case 3:
            jcs.in_color_space = JCS_RGB;
            break;
    }
    jpeg_set_defaults(&jcs);
    jpeg_set_quality(&jcs, 81, TRUE);
    jpeg_start_compress(&jcs, TRUE);

    while(jcs.next_scanline < jcs.image_height)
    {
        row_pointer[0] = &img_src[ jcs.next_scanline * 
                                   jcs.image_width * 
                                   jcs.input_components ];
        jpeg_write_scanlines(&jcs,row_pointer,1);
    }

    jpeg_finish_compress(&jcs);
    jpeg_destroy_compress(&jcs);
    fclose(out);

    return 0;
}

void writefile( const char* fname, void* data, size_t len )
{
    FILE* fp = fopen( fname, "wb" );
    if ( fp != NULL )
    {
        fwrite( data, 1, len, fp );
        fclose( fp );
    }
}

char* fmt2str( unsigned fmt )
{
    static char retstr[5] = {0};
    memset( retstr, 0, 5 );

    retstr[0] = fmt & 0xFF;
    retstr[1] = (fmt >> 8) & 0xFF;
    retstr[2] = (fmt >> 16) & 0xFF; 
    retstr[3] = (fmt >> 24) & 0xFF;

    return retstr;
}

void ctlGain( v4l_fd* cfd, size_t idx, float expr = 0.0f )
{
    if ( extCtlGain[idx] != NULL )
    {
        struct v4l2_control c = {0};
       
        int  maxv = extCtlGain[idx]->maximum;
        int  minv = extCtlGain[idx]->minimum;

        int newv = (float)maxv * expr;

        if ( newv < extCtlGain[idx]->minimum )
        {
            newv = extCtlGain[idx]->minimum;
        }
        else
        if ( newv > extCtlGain[idx]->maximum )
        {
            newv = extCtlGain[idx]->maximum;
        }

        c.id    = V4L2_CID_GAIN;
        c.value = newv * ( newv / extCtlGain[idx]->step );
        int ret = v4l_ioctl( cfd, VIDIOC_S_CTRL, &c );
        
        if ( ret < 0 ) 
        {
            perror( "EXPOSURE CONTROL" );
        }
        else
        {
#ifdef DEBUG
            printf( "Gain controlled : %X:%d/%d\n",
                    extCtlGain[idx]->id,
                    newv,
                    extCtlGain[idx]->maximum );
#endif /// of DEBUG
        }
    }
}

void ctlExposure( v4l_fd* cfd, size_t idx, float expr )
{
    if ( extCtlExposure[idx] != NULL )
    {
        struct v4l2_control c = {0};
       
        int  maxv = extCtlExposure[idx]->maximum;
        int  minv = extCtlExposure[idx]->minimum;

        int newv = (float)maxv* expr;

        if ( newv < extCtlExposure[idx]->minimum )
        {
            newv = extCtlExposure[idx]->minimum;
        }
        else
        if ( newv > extCtlExposure[idx]->maximum )
        {
            newv = extCtlExposure[idx]->maximum;
        }

        c.id    = V4L2_CID_EXPOSURE;
        c.value = extCtlExposure[idx]->step * 
                  ( newv / extCtlExposure[idx]->step );
        int ret = v4l_ioctl( cfd, VIDIOC_S_CTRL, &c );
        
        if ( ret < 0 ) 
        {
            perror( "EXPOSURE CONTROL" );
        }
        else
        {
#ifdef DEBUG
            printf( "Exposure controlled : %X:%d/%d\n",
                    extCtlExposure[idx]->id,
                    newv,
                    extCtlExposure[idx]->maximum );
#endif /// of DEBUG
        }
    }
#ifdef DEBUG
    else
    {
        printf( "no exposure control ?\n" );
    }
#endif /// of DEBUG
}

void getExtControls( v4l_fd* cfd, size_t idx )
{
    // find exposure control .
    struct v4l2_query_ext_ctrl* pec = new struct v4l2_query_ext_ctrl;

    memset( pec, 0, sizeof( struct v4l2_query_ext_ctrl ) );

    pec->id = V4L2_CID_EXPOSURE;

    int ret = v4l_query_ext_ctrl( cfd, pec, false, false );
    if ( ret >= 0 ) 
    {
        if ( extCtlExposure[idx] == NULL )
        {
            extCtlExposure[idx] = pec;
        }
        else
        {
            delete pec;
            pec = NULL;
        }
    }

    pec = new struct v4l2_query_ext_ctrl;

    memset( pec, 0, sizeof( struct v4l2_query_ext_ctrl ) );

    pec->id = V4L2_CID_EXPOSURE;

    ret = v4l_query_ext_ctrl( cfd, pec, true, false );
    if ( ret >= 0 ) 
    {
        if ( extCtlGain[idx] == NULL )
        {
            extCtlGain[idx] = pec;
        }
        else
        {
            delete pec;
            pec = NULL;
        }
    }

}

/////////////////////////////////////////////////////////////////////

void* threadCapture( void* p )
{
    if( btnCapture->active_r() > 0 )
    {
        btnCapture->deactivate();
    }

    Fl::lock();
    Fl_RGB_Image* imgCap = fl_imgtk::draw_widgetimage( winMain );
    Fl::unlock();

    if ( imgCap != NULL )
    {
        time_t cur_t;
        time( &cur_t );
        struct tm* cur_time = localtime( &cur_t );

        char mapfn[256] = {0};
        sprintf( mapfn,
                 "%s/mcsi2mon_capture_%04u%02u%02u_%02u%02u%02u.jpg",
                 strCapPath,
                 cur_time->tm_year + 1900,
                 cur_time->tm_mon + 1,
                 cur_time->tm_mday,
                 cur_time->tm_hour,
                 cur_time->tm_min,
                 cur_time->tm_sec );

        uchar* pdata = (uchar*)imgCap->data()[0];
        create_jpeg( mapfn, pdata, imgCap->w(), imgCap->h(), imgCap->d() );

        fl_imgtk::discard_user_rgb_image( imgCap );
    }
    
    btnCapture->activate();

    pthread_exit(0);
    return NULL;
}

void renderImage( size_t index )
{
    if ( ( bufferGrab[index] != NULL ) 
         && ( bufferGrabLock[index] == true ) )
    {
        unsigned perf1 = GetTickCount();   
#ifdef PERF_TEST
        unsigned testt0 = GetTickCount();
#endif /// of PERF_TEST

        size_t img_w = bufferSpec[index].width;
        size_t img_h = bufferSpec[index].height;
        size_t rgbsz = img_w * img_h;

        // need to check buffer size.
        if ( rgbsz == 0 )
        {
            bufferGrabLock[index] = false;
            return;
        }

        uchar* rgb = new uchar[ rgbsz * 3 ];
        uchar* gray = &bufferGrab[index][0];

        // need to check buffer pointers.
        if ( rgb == NULL )
        {
            bufferGrabLock[index] = false;
            return;
        }

        if ( gray == NULL )
        {
            delete[] rgb;
            rgb = NULL;

            bufferGrabLock[index] = false;
            return;
        }

        int zooming = btnZoom->value();
        int hdring  = btnHDR->value();

        if ( zooming == 0 )
        {
#ifdef GREEN_EFFECT
            #pragma omp parallel for shared(rgb, gray)
            for( size_t cnt=0; cnt<rgbsz; cnt++ )
            {
                unsigned g = gray[ cnt ] * 2;
                if ( g > 255 ) g = 255;
                rgb[ cnt * 3 + 0 ] = gray[ cnt ];
                rgb[ cnt * 3 + 1 ] = g;
                rgb[ cnt * 3 + 2 ] = gray[ cnt ];
            }
#else
            //#pragma omp parallel for shared(rgb, gray)
            #pragma omp parallel for
            for( size_t cnt=0; cnt<rgbsz; cnt++ )
            {
                rgb[ cnt * 3 + 0 ] = 
                rgb[ cnt * 3 + 1 ] = 
                rgb[ cnt * 3 + 2 ] = gray[ cnt ];
            }
#endif
        }
        else
        {
            // make cropped image as zooming.
            size_t sx = img_w / 3;
            size_t sy = img_h / 3;
            size_t ex = sx + ( img_w / 3 );
            size_t ey = sy + ( img_h / 3 );

            #pragma omp parallel for shared(rgb, gray)
            for( size_t cnty=sy; cnty<ey; cnty++ )
            {
                for( size_t cntx=sx; cntx<ex; cntx++ )
                {
                    size_t rq = ( cnty * img_w ) + cntx;
                    size_t wq = ( ( cnty-sy )*(img_w/3) ) + (cntx - sx);
                    rgb[ wq * 3 + 0 ] = gray[ rq ];
                    rgb[ wq * 3 + 1 ] = gray[ rq ];
                    rgb[ wq * 3 + 2 ] = gray[ rq ];
                }
            }

            img_w = img_w / 3;
            img_h = img_h / 3;
        }

#ifdef PERF_TEST
        unsigned testt1 = GetTickCount();
        printf( "#perftest: convert took %u ms.\n",
                testt1 - testt0 );
#endif /// of PERF_TEST

        boxCamera[index]->image( NULL );

        if ( imgCamera[index] != NULL )
        {
            fl_imgtk::discard_user_rgb_image( imgCamera[index] );
        }
                
        Fl_RGB_Image* tmpimg = new Fl_RGB_Image( rgb, img_w, img_h, 3 );

        if ( tmpimg != NULL )
        {
            // resize it ...
            float r = (float)boxCamera[index]->w() / (float)img_w;
            int new_w = img_w * r;
            int new_h = img_h * r;
#ifdef PERF_TEST
            testt0 = GetTickCount();
#endif /// of PERF_TEST
            imgCamera[index] = \
                fl_imgtk::rescale( tmpimg, new_w, new_h );
#ifdef PERF_TEST
            testt1 = GetTickCount();
            printf( "#perftest: resizing took %u ms.\n",
                    testt1 - testt0 );
#endif /// of PERF_TEST
            fl_imgtk::discard_user_rgb_image( tmpimg );

            // need HDR ?
            if ( hdring == 1 )
            {
                // may need big cost ....
                Fl_RGB_Image* imgsrc = imgCamera[index];
                imgCamera[index] = fl_imgtk::tonemapping_drago( imgsrc );
                if ( imgCamera[index] != NULL )
                {
                    fl_imgtk::discard_user_rgb_image( imgsrc );
                }
                else
                {
                    imgCamera[index] = imgsrc;
                }
            }

            //Draw Lines ...
            fl_imgtk::draw_line( imgCamera[index],
                                 new_w / 2,
                                 0,
                                 new_w / 2,
                                 new_h,
                                 0xFF00009F );
            fl_imgtk::draw_line( imgCamera[index],
                                 0,
                                 new_h / 2,
                                 new_w,
                                 new_h / 2,
                                 0xFF00009F );
            imgCamera[index]->uncache();
        }

        if ( imgCamera[index] != NULL )
        {
            boxCamera[index]->image( imgCamera[index] );
            boxCamera[index]->redraw();
        }

        unsigned perf2 = GetTickCount();

        sprintf( strPerf[index],
                 "Perf cap: %u ms, draw: %u ms",
                 grabPerf[index],
                 perf2 - perf1 );

        bufferGrabLock[index] = false;
        printf( "buffer grab %u unlocked\n", index );
        fflush( stdout );
    }
}

void* threadRender( void* p )
{
    while ( true )
    {
        pthread_mutex_lock( &ptMtx[0] );
        pthread_mutex_lock( &ptMtx[1] );

        // wait for a sec.
        struct timespec wtime = { 1, 0 };

        int reti[2] = {0};

        reti[0] = pthread_cond_timedwait( &ptSig[0],
                                          &ptMtx[0],
                                          &wtime );

        reti[1] = pthread_cond_timedwait( &ptSig[1],
                                          &ptMtx[1],
                                          &wtime );

        pthread_mutex_unlock( &ptMtx[0] );
        pthread_mutex_unlock( &ptMtx[1] );

        if ( threadalive == false )
        {
            pthread_exit( 0 );
        }
       
        #pragma omp parallel for num_threads(2)
        for( size_t cnt=0; cnt<2; cnt++ )
        {
            if ( reti[cnt] != ETIMEDOUT )
            {
                Fl::lock();
                renderImage( cnt );
                Fl::unlock();
            }
        }

        //winMain->redraw();

        // this is reason double free from X11. Why?
        Fl::awake();

        if ( threadalive == false )
            break;
    }

    pthread_exit( 0 );
    return NULL;
}

void* threadGrab( void* p )
{
    if ( p == NULL )
        pthread_exit( NULL );

    threadopt* popt = (threadopt*)p;
    threadopt topt = {0};
    memcpy( &topt, popt, sizeof( threadopt ) );
    delete popt;

    int         ret;
    size_t      fmtplanes = 1;
    char        dev_fn[80] = {0};
#if defined(FEATURE_NKISP)
    size_t      idx = topt.devindex / 5;
#else
    size_t      idx = topt.devindex / 4;
#endif

    sprintf( dev_fn, "/dev/video%u", topt.devindex );

#ifdef DEBUG_GRAB
    printf( "camera : %s\n",  dev_fn );
#endif /// of DEBUG_GRAB

    v4l_fd_init( &camera_fd[idx] );

    // ---
    int realfd = v4l_open( &camera_fd[idx], dev_fn, true );
    if ( realfd < 0 )
    {
        perror( "device open" );
        pthread_exit( NULL );
    }
#ifdef DEBUG_GRAB
    printf( "Camera[%u].Open Ok.\n", topt.devindex );
#endif /// of DEBUG_GRAB

    // ---
    struct v4l2_format vfmt = {0};
    unsigned captype = v4l_determine_type( &camera_fd[idx] );
    unsigned memtype = V4L2_MEMORY_MMAP;

    v4l_g_fmt( &camera_fd[idx], &vfmt, captype );

    unsigned pixelformat = v4l_format_g_pixelformat( &vfmt );
    unsigned plane = v4l_format_g_num_planes( &vfmt );
    unsigned img_w = v4l_format_g_width( &vfmt );
    unsigned img_h = v4l_format_g_height( &vfmt );

    bufferSpec[idx].width = img_w;
    bufferSpec[idx].height = img_h;
    bufferSpec[idx].depth = 1;
    bufferGrab[idx] = new uchar[ img_w * img_h ];

    if ( pixelformat != SUPPORTED_FMT )
    {
        strCapError[idx] = "ERROR:\nNot supported Pixel format!";
        v4l_close( &camera_fd[idx] );
        boxCOverlay[idx]->label( strCapError[idx].c_str() );
        boxCOverlay[idx]->redraw();
        pthread_exit( 0 );
    }

    // --
    // find exposure control .
    getExtControls( &camera_fd[idx], idx );

    Fl::repeat_timeout( 0.1f, fl_tcb, NULL );
    
    // --
    v4l_buffer vbuffer;
    v4l_queue  vqueue;
    bool       verror = false;

    v4l_queue_init( &vqueue, captype, memtype );

    ret = v4l_queue_reqbufs( &camera_fd[idx], &vqueue, REQ_BUFF_SZ );
    if ( ret < 0 ) 
    {
        perror( "v4l_queue_reqbufs()" );
        verror = true;
    }

    ret = v4l_queue_obtain_bufs( &camera_fd[idx], &vqueue, 0 );
    if ( ret < 0 )
    {
        perror( "v4l_queue_obtain_bufs()" );
        verror = true;
    }

    unsigned bsz = v4l_queue_g_buffers( &vqueue );

    for( unsigned cnt=0; cnt<bsz; cnt++ )
    {
        v4l_queue_buffer_init( &vqueue, &vbuffer, cnt );
        v4l_buffer_qbuf( &camera_fd[idx], &vbuffer );
    }

#ifdef DEBUG_GRAB
    printf( "Ok.\n" );
#endif /// of DEBUG_GRAB

    // make stream on !
    ret = v4l_ioctl( &camera_fd[idx], VIDIOC_STREAMON, &vqueue.type );
    if ( ret < 0 )
    {
        perror( "v4l_ioctrl(VIDIOC_STREAMON)" );
        verror = true;
    }

    // wait a sec for stable works.
    // usleep( 500 * 1000 );

    if ( verror == true )
    {
        boxCOverlay[idx]->label( "ERROR" );
        boxCOverlay[idx]->redraw();
    }

    // ---
#ifdef DEBUG_GRAB
    printf( "Camera[%u].Now polling.\n", topt.devindex );
#endif /// of DEBUG_GRAB

    // Exposure Control ...
    ctlExposure( &camera_fd[idx], idx, EXPOSURE_R );

    size_t c_que = 0;

    while( threadalive == true )
    {
        if ( threadalive == false )
            break;

        fd_set fds;
        struct timeval tv;

        unsigned perf0 = GetTickCount();
        unsigned perf1 = perf0;

        FD_ZERO( &fds );
        FD_SET( realfd, &fds );
        tv.tv_sec = 5; /// set to 5 second to wait.
        tv.tv_usec = 0;

        ret = select( realfd + 1, &fds, NULL, NULL, &tv );

        if ( ret <= 0 )
        {
#ifdef DEBUG_GRAB
            printf( "camera[%u] select failure.\n", topt.devindex );
#endif /// of DEBUG_GRAB
            boxCOverlay[idx]->label( "Wait sync failure.");

            usleep( 100 * 1000 );
        }
        else
        {
            boxCOverlay[idx]->label( NULL );

            v4l_queue_buffer_init( &vqueue, &vbuffer, 0 );

            ret = v4l_buffer_dqbuf( &camera_fd[idx], &vbuffer );
            if ( ret < 0 ) perror( "v4l_buffer_dqbuf()" );

            perf1 = GetTickCount();

            unsigned bufflen = v4l_queue_g_length( &vqueue, 0 );
            if ( bufflen > 0 )
            {
                unsigned bidx = vbuffer.buf.index;

                void* ptrData = v4l_queue_g_mmapping( &vqueue,
                                                      bidx, 
                                                      0 );
                if ( ptrData != NULL )
                {
                    grabPerf[idx] = perf1 - perf0;

                    if( bufflen <= img_w * img_h )
                    {
                        if ( bufferGrabLock[idx] == false )
                        {
                            memset( bufferGrab[idx], 0, img_w * img_h );

                            bufferGrabLock[idx] = true;

                            memcpy( bufferGrab[idx], 
                                    ptrData, 
                                    bufflen );
                        }
                    }
                }
            }

            ret = v4l_buffer_qbuf( &camera_fd[idx], &vbuffer );
            if ( ret < 0 ) perror( "v4l_buffer_qbuf()" );

            pthread_cond_signal( &ptSig[idx] );
        }
    }

#ifdef DEBUG_GRAB
    printf( "camera[%u] finalizing ...\n", topt.devindex );
    fflush( stdout );
#endif /// of DEBUG_GRAB

    // ---
    v4l_queue_free( &camera_fd[idx], &vqueue );
    v4l_queue_munmap_bufs( &camera_fd[idx], &vqueue, REQ_BUFF_SZ );
    v4l_close( &camera_fd[idx] );

#ifdef DEBUG_GRAB
    printf( "done[%u]\n", topt.devindex );
#endif /// of DEBUG_GRAB

    pthread_exit( 0 );
    return NULL;
}

/////////////////////////////////////////////////////////////////////

Fl_RGB_Image* loadIcon( Fl_Window* w )
{
    if ( w == NULL )
        return NULL;

    string trypath = "/usr/local/share/icons/hicolor/256x256/apps";
    string fpath = trypath + "/mcsi2mon.png";
    bool   existed = false;

    if ( access( fpath.c_str() , 0 ) == 0 )
    {
        existed = true;
    }
    else
    {
        trypath = getenv( "PWD" );
        fpath = trypath + "/res/mcsi2mon.png";

        if ( access( fpath.c_str(), 0 ) == 0 )
        {
            existed = true;
        }
    }

    if ( existed == true )
    {
        Fl_RGB_Image* imgIcon = new Fl_PNG_Image( fpath.c_str() );
        if ( imgIcon != NULL )
        {
            w->icon( imgIcon );
            return imgIcon;
        }
    }

    return NULL;
}

void createCompo()
{
    unsigned colBg = 0x333333FF;
    unsigned colFg = 0xFF6633FF;
    unsigned lblSz = 12;

    string appTitle = "MIPI CSI2 Stereo Camera Testing App. ";
    appTitle += " | version ";
    appTitle += DEF_APP_VERSION;

    int w_x = 0;
    int w_y = 0;
    int w_w = 0;
    int w_h = 0;

    Fl::screen_work_area( w_x, w_y, w_w, w_h );

    w_h -= 20;

    winMain = new Fl_Double_Window( w_x, w_y, w_w, w_h, appTitle.c_str() );
    if ( winMain != NULL )
    {
        imgIcon = loadIcon( winMain );
        winMain->color( colBg );
        winMain->labelcolor( colFg );
        winMain->labelsize( lblSz );
        winMain->labelfont( FL_HELVETICA );

        int put_w = ( w_w / 2 ) - 20;
        int put_h = (float)put_w * (3.f / 4.f);
        int put_x = 10;
        int put_y = 10;

        for( size_t cnt=0; cnt<2; cnt++ )
        {
            boxCamera[cnt] = new Fl_Box( put_x, put_y, put_w, put_h );
            if( boxCamera[cnt] != NULL )
            {
                boxCamera[cnt]->box( FL_FLAT_BOX );
                boxCamera[cnt]->align( FL_ALIGN_INSIDE );
                boxCamera[cnt]->color( 0 );
                boxCamera[cnt]->image( NULL );
            }

            boxCOverlay[cnt] = new Fl_Box( put_x, put_y, put_w, put_h,
                                           "-- PREPARING CAMERA -- " );
            if( boxCOverlay[cnt] != NULL )
            {
                boxCOverlay[cnt]->box( FL_NO_BOX );
                boxCOverlay[cnt]->align( FL_ALIGN_INSIDE );
                boxCOverlay[cnt]->labelfont( winMain->labelfont() );
                boxCOverlay[cnt]->labelsize( winMain->labelsize() + 4 );
                boxCOverlay[cnt]->labelcolor( 0xFF333300 );
                boxCOverlay[cnt]->image( NULL );
            }

            boxPerf[cnt] = new Fl_Box( put_x, put_y + put_h + 5,
                                       put_w, 20 );
            if ( boxPerf[cnt] != NULL )
            {
                sprintf( strPerf[cnt], "-- performance [%u] --", cnt );
                boxPerf[cnt]->box( FL_FLAT_BOX );
                boxPerf[cnt]->color( winMain->color() );
                boxPerf[cnt]->labelfont( FL_COURIER );
                boxPerf[cnt]->labelsize( winMain->labelsize() );
                boxPerf[cnt]->labelcolor( winMain->labelcolor() );
                boxPerf[cnt]->label( strPerf[cnt] );
                put_x += put_w + 15;
            }
        }
 
        put_y += put_h + 30;
        put_h = 20;
        put_w = ( w_w / 5 ) - 20;
        put_x = w_w - ( ( put_w * 4 ) + 20 );

        btnCapture = new Fl_Button( put_x, put_y, put_w, put_h,
                                    "&Capture window to JPEG." );
        if ( btnCapture != NULL )
        {
            btnCapture->color( winMain->color(), 
                               fl_darker( winMain->color() ) );
            btnCapture->labelfont( winMain->labelfont() );
            btnCapture->labelcolor( winMain->labelcolor() );
            btnCapture->labelsize( winMain->labelsize() );
            btnCapture->clear_visible_focus();
            btnCapture->callback( fl_wcb, NULL );

            put_x += put_w + 10;
        }

        btnGrabRaw = new Fl_Button( put_x, put_y, put_w, put_h,
                                    "&Grab each frames into PNG." );
        if ( btnGrabRaw != NULL )
        {
            btnGrabRaw->color( winMain->color(), 
                               fl_darker( winMain->color() ) );
            btnGrabRaw->labelfont( winMain->labelfont() );
            btnGrabRaw->labelcolor( winMain->labelcolor() );
            btnGrabRaw->labelsize( winMain->labelsize() );
            btnGrabRaw->clear_visible_focus();
            btnGrabRaw->callback( fl_wcb, NULL );
            btnGrabRaw->deactivate();

            put_x += put_w + 10;
        }

        btnZoom = new Fl_Check_Button( put_x, put_y, put_w, put_h,
                                       "Enable center zoom" );
        if ( btnZoom != NULL )
        {
            btnZoom->color( winMain->color(), 
                            fl_darker( winMain->color() ) );
            btnZoom->labelfont( winMain->labelfont() );
            btnZoom->labelcolor( winMain->labelcolor() );
            btnZoom->labelsize( winMain->labelsize() );
            btnZoom->clear_visible_focus();
            btnZoom->callback( fl_wcb, NULL );

            put_x += put_w + 10;
        }

        btnHDR = new Fl_Check_Button( put_x, put_y, put_w, put_h,
                                      "Enable auto HDR" );
        if ( btnHDR != NULL )
        {
            btnHDR->color( winMain->color(), 
                            fl_darker( winMain->color() ) );
            btnHDR->labelfont( winMain->labelfont() );
            btnHDR->labelcolor( winMain->labelcolor() );
            btnHDR->labelsize( winMain->labelsize() );
            btnHDR->clear_visible_focus();
            btnHDR->callback( fl_wcb, NULL );

            put_y += put_h + 5;
        }

        put_x = 110;
        put_w = ( w_w / 2 ) - 20 - 100;

        sldExposure = new Fl_VSliderEx( put_x, put_y, put_w, put_h,
                                        "Exposure :" );

        if( sldExposure != NULL )
        {
            sldExposure->box( FL_UP_BOX );
            sldExposure->align( FL_ALIGN_LEFT );
            sldExposure->valueboxwh( 100 );
            sldExposure->precision( 0 );
            sldExposure->slider_size( 0.1 );
            sldExposure->color( winMain->color(), winMain->color() );
            sldExposure->labelfont( winMain->labelfont() );
            sldExposure->labelcolor( winMain->labelcolor() );
            sldExposure->labelsize( winMain->labelsize() );
            sldExposure->textfont( winMain->labelfont() );
            sldExposure->textcolor( winMain->labelcolor() );
            sldExposure->textsize( winMain->labelsize() );
            sldExposure->clear_visible_focus();
            sldExposure->maximum( 65535 );
            sldExposure->minimum( 1 );
            sldExposure->value( 1 );
            sldExposure->when( FL_WHEN_RELEASE );
            sldExposure->callback( fl_wcb, NULL );

            put_x += put_w + 100;
        } 

        sldGain = new Fl_VSliderEx( put_x, put_y, put_w, put_h,
                                    "Gain :" );

        if( sldGain != NULL )
        {
            sldGain->box( FL_UP_BOX );
            sldGain->align( FL_ALIGN_LEFT );
            sldGain->valueboxwh( 100 );
            sldGain->precision( 0 );
            sldGain->slider_size( 0.1 );
            sldGain->color( winMain->color(), winMain->color() );
            sldGain->labelfont( winMain->labelfont() );
            sldGain->labelcolor( winMain->labelcolor() );
            sldGain->labelsize( winMain->labelsize() );
            sldGain->textfont( winMain->labelfont() );
            sldGain->textcolor( winMain->labelcolor() );
            sldGain->textsize( winMain->labelsize() );
            sldGain->clear_visible_focus();
            sldGain->maximum( 512 );
            sldGain->minimum( 0 );
            sldGain->value( 0 );
            sldGain->when( FL_WHEN_RELEASE );
            sldGain->callback( fl_wcb, NULL );
            sldGain->deactivate();

            put_y += put_h + 5;
            put_x = 10;
        } 

        // resize window in height.
        w_h = put_y + 30;
        winMain->resize( w_x, w_y, w_w, w_h );

        // Copyright mark ...

        put_x = 10;
        put_y = w_h - 20;
        put_h = 20;
        put_w = w_w - 20;

        Fl_Box* boxCR = new Fl_Box( put_x, put_y, put_w, put_h );
        if ( boxCR != NULL )
        {
            boxCR->box( FL_NO_BOX );
            boxCR->color( winMain->color() );
            boxCR->labelcolor( winMain->labelcolor() );
            boxCR->align( FL_ALIGN_INSIDE | FL_ALIGN_RIGHT );
            boxCR->label( "(C)Copyright 2019 Raphael Kim | rageworx@@gmail.com" );
            boxCR->deactivate();
        }

        winMain->end();
        winMain->callback( fl_wcb, NULL );
        winMain->show();
    }
}

void stopThreads()
{
    if ( threadalive == false )
        return;

    // toggle thread can terminated ...
    threadalive = false;

    // send signal for render thread to read 'threadalive'.
    pthread_cond_signal( &ptSig[0] );
    pthread_cond_signal( &ptSig[1] );

    // Let's wait for threads stop.
    // do not make force-thread kill, it becomes reason of system halt.
    pthread_join( ptGrab[0], 0 );
    pthread_join( ptGrab[1], 0 );
    pthread_join( ptRender, 0 );
}

void fl_wcb( Fl_Widget* w, void* p )
{
    if ( w == btnCapture )
    {
        btnCapture->deactivate();

        pthread_t ptCapInst = 0;
        pthread_create( &ptCapInst, NULL, threadCapture, NULL );
        return;
    }

    if ( w == sldExposure )
    {
        float mv = (float)sldExposure->maximum();
        float rv = (float)sldExposure->value();

        //printf ( "mv = %f, rv = %f\n", mv, rv );

        if ( ( mv < 0 ) || ( rv < 0 ) )
            return;

        for( size_t cnt=0; cnt<2; cnt++ )
        {
            if ( enableCtl[cnt] == true )
            {
                float newv = rv / mv;
#ifdef DEBUG
                printf( "fl_wcb::exposure control to %f ..\n", newv );
#endif /// of DEBUG
                ctlExposure( &camera_fd[cnt], cnt, newv );
            }
        }
        
        return;
    }

    if ( w == winMain )
    {
        w->deactivate();
        w->redraw();
     
        Fl::unlock();
        Fl::flush();

        stopThreads();

        w->hide();

        return;
    }
}

void fl_tcb( void* p )
{
    // checks external controls.
 
    Fl::lock();

    if ( ( extCtlExposure[0] != NULL ) && ( extCtlExposure[1] != NULL ) )
    { 
        sldExposure->maximum( (double)extCtlExposure[0]->maximum );
        sldExposure->minimum( (double)extCtlExposure[0]->minimum );
        sldExposure->value( (double)extCtlExposure[0]->maximum * 
                            EXPOSURE_R );
 
        if ( sldExposure->active_r() == 0 )
            sldExposure->activate(); 

        sldExposure->redraw();
    }

    if ( ( extCtlGain[0] != NULL ) && ( extCtlGain[1] != NULL ) )
    {
        sldGain->maximum( (double)extCtlGain[0]->maximum );
        sldGain->minimum( (double)extCtlGain[0]->maximum );
        sldGain->value( 0 );

        if ( sldGain->active_r() == 0 )
            sldGain->activate();

        sldGain->redraw();
    }

    for( size_t cnt=0; cnt<2; cnt++ )
    {
        if ( ( sldExposure->active_r() > 0 ) && ( sldGain->active_r() > 0 ) )
        { 
            enableCtl[cnt] = true;
        }
    }

    Fl::unlock();
}

int setMutex()
{
    semthis = sem_open( SEM_NAME,
                        O_CREAT | O_EXCL,
                        S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH,
                        1 );

    if ( semthis == SEM_FAILED )
    {
        if ( errno == EEXIST )
        {
            return -1;
        }
    }

    return 0;
}

void closeMutex()
{
    if ( semthis != NULL )
    {
        sem_close( semthis );
        sem_unlink( SEM_NAME );
    }
}

void sigHandler( int Sig )
{
    if ( sigmutex == false )
    {
        sigmutex = true;

        printf( "sigHandler ( %d )!\n", Sig );
        fflush( stdout );

        //fl_wcb( winMain, NULL );
        winMain->hide();

#ifndef USE_MUTEX
        closeMutex();
#endif /// USE_MUTEX

        exit( 0 );
    }
}


/////////////////////////////////////////////////////////////////////

int main( int argc, char** argv )
{
#ifdef USE_MUTEX
    // check Mutex
    if ( setMutex() < 0 )
    {
        // duplicated run.
        perror( "duplicated run!" );
        return -1;
    }
#endif /// of USE_MUTEX

    signal( SIGINT, sigHandler );
    signal( SIGABRT, sigHandler );

    sprintf( strCapPath, "%s/Pictures", getenv("HOME") );

    Fl::scheme( "flat" );
    Fl::lock();

    createCompo();

    Fl::add_timeout( 0.1f, fl_tcb, NULL );

    // Create Render threads.
    pthread_create( &ptRender, NULL, threadRender, NULL );

    // Create grab threads
    for( size_t cnt=0; cnt<2; cnt++ )
    {
        threadopt* opt = new threadopt;
        if ( opt != NULL )
        {
            memset( opt, 0, sizeof( threadopt ) );
#if defined(FEATURE_NKISP)
            opt->devindex = cnt * 5;
#else
            opt->devindex = cnt * 4;
#endif
            pthread_create( &ptGrab[cnt], NULL, threadGrab, opt );
        }
    }

    int reti = Fl::run();

    // sometimes FLTK cannot remove window,
    // so I used this trick.
    Fl::flush();

    for( size_t cnt=0; cnt<2; cnt++ )
    {
        if ( bufferGrab[cnt] != NULL )
        {
            delete[] bufferGrab[cnt];
            bufferGrab[cnt] = NULL;
        }

        fl_imgtk::discard_user_rgb_image( imgCamera[cnt] );
    }

    Fl::remove_timeout( fl_tcb, NULL );

#ifdef USE_MUTEX
    closeMutex();
#endif

    fl_imgtk::discard_user_rgb_image( imgIcon );

    return reti;
}
