#include <FL/Fl.H>
#include "Fl_VSliderEx.H"
#include <FL/fl_draw.H>
#include <math.h>

/**
  Creates a new Fl_Value_Slider widget using the given
  position, size, and label string. The default boxtype is FL_DOWN_BOX.
*/
Fl_VSliderEx::Fl_VSliderEx(int X, int Y, int W, int H, const char*l)
: Fl_Slider(X,Y,W,H,l) 
{
  step(1,100);
  textfont_ = FL_HELVETICA;
  textsize_ = 10;
  textcolor_ = FL_FOREGROUND_COLOR;
  type( FL_HOR_SLIDER );
}

void Fl_VSliderEx::valueboxwh( unsigned wh ) 
{ 
    if ( horizontal() )
        if( wh<35 ) wh=35; 
    else
        if( wh<25 ) wh=25;

    vsz_wh = wh; 
}

void Fl_VSliderEx::draw() 
{
  int sxx = x(), syy = y(), sww = w(), shh = h();
  int bxx = x(), byy = y(), bww = w(), bhh = h();
  if (horizontal()) {
    bww = vsz_wh; sxx += vsz_wh; sww -= vsz_wh;
  } else {
    syy += vsz_wh; bhh = vsz_wh; shh -= vsz_wh;
  }
  if (damage()&FL_DAMAGE_ALL) draw_box(box(),sxx,syy,sww,shh,color());
  Fl_Slider::draw(sxx+Fl::box_dx(box()),
		  syy+Fl::box_dy(box()),
		  sww-Fl::box_dw(box()),
		  shh-Fl::box_dh(box()));
  draw_box(box(),bxx,byy,bww,bhh,color());
  char buf[128];
  format(buf);
  fl_font(textfont(), textsize());
  fl_color(active_r() ? textcolor() : fl_inactive(textcolor()));
  fl_draw(buf, bxx, byy, bww, bhh, FL_ALIGN_CLIP);
}

int Fl_VSliderEx::handle(int event) 
{
  if (event == FL_PUSH && Fl::visible_focus()) {
    Fl::focus(this);
    redraw();
  }
  int sxx = x(), syy = y(), sww = w(), shh = h();
  if (horizontal()) {
    sxx += vsz_wh; sww -= vsz_wh;
  } else {
    syy += vsz_wh; shh -= vsz_wh;
  }
  return Fl_Slider::handle(event,
			   sxx+Fl::box_dx(box()),
			   syy+Fl::box_dy(box()),
			   sww-Fl::box_dw(box()),
			   shh-Fl::box_dh(box()));
}
