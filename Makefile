CC=gcc
CXX=g++
AR=ar

# Installation pathes
INST_PATH = /usr/local/bin
DESK_PATH = /usr/share/applications
IRES_PATH = /usr/local/share/icons/hicolor/256x256/apps

# FLTK configs
FCG = fltk-config --use-images
FLTKCFG_CXX := $(shell ${FCG} --cxxflags)
FLTKCFG_LFG := $(shell ${FCG} --ldflags)

# FLTK image toolkit
FLIMGTK = ../fl_imgtk/lib

V4LPATH = ../v4l-utils
V4LINCS = ${V4LPATH}/include
V4LUTILS = ${V4LPATH}/utils
V4LUCOMMON = ${V4LUTILS}/common

OPTFLG = -O2 -ffast-math -fexceptions -fopenmp -mtune=native -s
#OPTFLG = -g -ffast-math -fexceptions -fopenmp -mtune=native

#CFLAGS += -DDEBUG -DDEBUG_GRAB
CFLAGS += ${OPTFLG}
CFLAGS += -DFEATURE_NKISP
CFLAGS += -I${V4LINCS}
CFLAGS += -I${V4LUCOMMON}
CFLAGS += -I${FLIMGTK}
CFLAGS += ${FLTKCFG_CXX}

LFLAGS += ${OPTFLG}
LFLAGS += -L${FLIMGTK}
LFLAGS += -lfl_imgtk
LFLAGS += ${FLTKCFG_LFG}

SRCS = $(wildcard src/*.cpp)
OBJS = $(SRCS:src/%.cpp=obj/%.o)
BPTH = bin/
BIN  = mcsi2mon

all: prepare $(BPTH)$(BIN)

prepare:
	@mkdir -p obj
	@mkdir -p bin

$(OBJS): obj/%.o : src/%.cpp
	@$(CXX) $(CFLAGS) -c $< -o $@

$(BPTH)$(BIN): $(OBJS)
	@$(CXX) $(OBJS) $(LFLAGS) -o $@
	@sync

clean:
	@rm -rf obj/$(OBJS)
	@rm -rf bin/$(BIN)

install: $(BPTH)$(BIN)
	@cp -rf $< $(INST_PATH)
	@mkdir -p $(IRES_PATH)
	@cp -rf res/mcsi2mon.png $(IRES_PATH)
	@./mkinst.sh $(INST_PATH) $(DESK_PATH)

uninstall:
	@rm -rf $(INST_PATH)/$(BIN)
	@rm -rf $(IRES_PATH)/mcsi2mon.png
	@rm -rf $(DESK_PATH)/mcsi2mon.desktop
