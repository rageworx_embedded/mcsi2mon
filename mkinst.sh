#!/bin/bash

replaceinfile()
{
    local fnsrc=$1
    local fndst=$2
    local dstr=$3
    local rstr=$4
    
    if [ -f ${fnsrc} ]; then
        sed "s|${dstr}|${rstr}|g" ${fnsrc} > ${fndst}
        return 1
    else
        return 0
    fi
}

APP_NM=mcsi2mon
VER_GREP=`grep DEF_APP_VERSION src/version.h`
VER_ARR=($VER_GREP)
VER_STR=`echo ${VER_ARR[2]} | sed "s/\"//g"`

INSTPATH=$1
DESKPATH=$2

if [ "$INSTPATH" == "" ];then
    echo "Requires installation path."
    exit 0
fi

if [ "$DESKPATH" == "" ]; then
    DESKPATH=${HOME}/.local/share/applications
fi

echo "Installing to ${INSTPATH} ... "

SRCRES="res/${APP_NM}.desktop"
DSTRES="${DESKPATH}/${APP_NM}.desktop"
TMPRES=_tmpres_

if [ ! -f ${SRCRES} ];then
    echo "Requires ${SRCRES} file."
    exit 0
fi

replaceinfile ${SRCRES} ${TMPRES} "##INSTALLEDPATH##" ${INSTPATH}
replaceinfile ${TMPRES} ${DSTRES} "##PROGRAMVERSION##" ${VER_STR}
rm -rf ${TMPRES}

echo "done."
