#MCSI2MON

RK3399 MIPI CSI 2 stereo camera testing program for AR0521

## Default requirements

* RK3399 !
* Base board designed from reference RK3399 Excavator, or Rock960B,C
* Dual MIPI CSI2, gray-scale color space only.

## Required libraries

* v4l-utils git clonned source code in same directory level ( no need to build it )
* fltk-1.3.5-2-ts be installed.
* fl_imgtk in same directory and need to be built.


## Install to desktop application

* build source first with ```make```
* then install with ```sudo make install```.
